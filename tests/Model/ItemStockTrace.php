<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Tests\Model;

/**
 * Description of ItemStockTrace
 *
 * @author apple
 */
class ItemStockTrace extends Item implements \Kematjaya\ItemPackBundle\Entity\StockInterface
{
    //put your code here
    public function isCountStock(): bool 
    {
        return true;
    }

}
