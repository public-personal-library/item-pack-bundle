<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Tests\Model;

/**
 * Description of ItemStore
 *
 * @author programmer
 */
class ItemStore implements \Kematjaya\ItemPackBundle\Entity\ItemStoreInterface 
{
    private $quantity;
    
    public function getItem(): \Kematjaya\ItemPackBundle\Entity\ItemInterface 
    {
        
    }

    public function getQuantity(): float 
    {
        return (float)$this->quantity;
    }

    public function setQuantity(float $quantity)
    {
        $this->quantity = $quantity;
    }
    
    public function getStore(): \Kematjaya\ItemPackBundle\Entity\StoreInterface 
    {
        
    }

}
