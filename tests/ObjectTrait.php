<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPTrait.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Tests\Model\Item;
use Kematjaya\ItemPackBundle\Tests\Model\Packaging;
use Kematjaya\ItemPackBundle\Tests\Model\ItemPackage;

/**
 *
 * @author programmer
 */
trait ObjectTrait {
    
    public function buildObject(): Item
    {
        $item = (new Item())
                ->setPrincipalPrice(1000)
                ->setCode('test')
                ->setName('Test')
                ->setLastPrice(1200)
                ->setLastStock(0)
                ->setUseBarcode(false);
        
        return $item;
    }
    
    public function buildObjectStock(): Model\ItemStockTrace
    {
        $item = (new Model\ItemStockTrace())
                ->setPrincipalPrice(1000)
                ->setCode('test')
                ->setName('Test')
                ->setLastPrice(1200)
                ->setLastStock(0)
                ->setUseBarcode(false);
        
        return $item;
    }
    
    public function buildPackaging(): Packaging
    {
        return (new Packaging())->setCode('pcs')->setName('PCS');
    }
    
    public function buildItemPackage(Item $item):ItemPackage
    {
        $packaging = $this->buildPackaging();
        
        return (new ItemPackage())
                ->setItem($item)
                ->setPackaging($packaging)
                ->setQuantity(1)
                ->setPrincipalPrice($item->getPrincipalPrice())
                ->setSalePrice($item->getLastPrice());
    }
    
}
