<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\ItemPackBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class AppKernelTest extends Kernel
{
    /**
     * 
     * @return array
     */
    public function registerBundles()
    {
        return [
            new ItemPackBundle(),
            new FrameworkBundle()
        ];
    }
    
    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) use ($loader) {
            $loader->load(__DIR__ .'/config/config.yml');
            
            $container->addObjectResource($this);
        });
    }
}
