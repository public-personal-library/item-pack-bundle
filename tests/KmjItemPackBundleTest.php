<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
abstract class KmjItemPackBundleTest extends WebTestCase
{
    
    public function testInitBundle(): ContainerInterface
    {
        $client = static::createClient();
        $container = $client->getContainer();
        
        $this->assertInstanceOf(ContainerInterface::class, $container);
        
        return $container;
    }
    
    public static function getKernelClass() :string
    {
        return AppKernelTest::class;
    }
}
