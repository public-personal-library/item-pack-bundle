<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Exception\NotSufficientStockException;
use Kematjaya\ItemPackBundle\Exception\PackageEmptyException;
use Kematjaya\ItemPackBundle\Service\StockServiceInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockServicesTest extends WebTestCase 
{
    use ObjectTrait;
    
    public static function getKernelClass():string 
    {
        return AppKernelTest::class;
    }
    
    public function testInstance():StockServiceInterface
    {
        $container = $this->getContainer();
        $this->assertTrue($container->has(StockServiceInterface::class));
        
        return $container->get(StockServiceInterface::class);
    }
    
    /**
     * @depends testInstance
     */
    public function testStockServiceException(StockServiceInterface $service)
    {
        $this->expectException(PackageEmptyException::class);
        $item = $this->buildObject();
        $service->addStock($item, 10);
    }
    
    /**
     * @depends testInstance
     */
    public function testAddStockServiceSuccess(StockServiceInterface $service)
    {
        $item = $this->buildObject();
        $itemPackage = $this->buildItemPackage($item);
        $item->addItemPackage($itemPackage);
        
        $expect1 = $item->getLastStock() + 10;
        $service->addStock($item, 10);
        $this->assertEquals($item->getLastStock(), $expect1);
        
        $expect2 = $item->getLastStock() + 10;
        $service->addStock($item, 10);
        $this->assertEquals($item->getLastStock(), $expect2);
    }
    
    /**
     * @depends testInstance
     */
    public function testGetStockServiceException(StockServiceInterface $service)
    {
        $item = $this->buildObject();
        $itemPackage = $this->buildItemPackage($item);
        $item->addItemPackage($itemPackage);
        
        $result = $service->getStock($item, 10);
        $this->assertEquals(-10, $result->getLastStock());
        
        $itemStock = $this->buildObjectStock();
        $itemStock->addItemPackage(
            $this->buildItemPackage($itemStock)
        );
        $this->expectException(NotSufficientStockException::class);
        $service->getStock($itemStock, 10);
    }
    
    /**
     * @depends testInstance
     */
    public function testGetStockServiceSuccess(StockServiceInterface $service)
    {
        $item = $this->buildObject();
        $itemPackage = $this->buildItemPackage($item);
        $item->addItemPackage($itemPackage);
        
        $service->addStock($item, 10);
        
        $expect = $item->getLastStock() - 5;
        $service->getStock($item, 5);
        $this->assertEquals($item->getLastStock(), $expect);
    }
}
