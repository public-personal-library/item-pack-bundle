<?php

namespace Kematjaya\ItemPackBundle\Tests\Repository;

use Kematjaya\ItemPackBundle\Tests\Model\Item;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemRepository implements \Kematjaya\ItemPackBundle\Repository\ItemRepositoryInterface
{
    
    public function createItem(): ItemInterface 
    {
        return new Item();
    }

    public function save(ItemInterface $item): void 
    {
        
    }

}
