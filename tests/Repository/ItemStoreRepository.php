<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Tests\Repository;

use Kematjaya\ItemPackBundle\Entity\StoreInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Repository\ItemStoreRepositoryInterface;
use Kematjaya\ItemPackBundle\Entity\ItemStoreInterface;

/**
 * Description of ItemStoreRepository
 *
 * @author apple
 */
class ItemStoreRepository implements ItemStoreRepositoryInterface 
{
    //put your code here
    public function findOneByItemAndStore(ItemPackageInterface $itemPackage, StoreInterface $store): ?ItemStoreInterface 
    {
        return new \Kematjaya\ItemPackBundle\Tests\Model\ItemStore();
    }

    public function save(ItemStoreInterface $itemStore, float $quantity, \Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface $entity = null): void 
    {
        $qty = $itemStore->getQuantity() + $quantity;
        $itemStore->setQuantity($qty);
    }

}
