<?php

namespace Kematjaya\ItemPackBundle\Tests\Repository;

use Kematjaya\ItemPackBundle\Tests\Model\Packaging;
use Kematjaya\ItemPackBundle\Tests\Model\ItemPackage;
use Kematjaya\ItemPackBundle\Repository\ItemPackageRepositoryInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackageRepository implements ItemPackageRepositoryInterface
{
    
    public function createPackage(ItemInterface $item): ItemPackageInterface 
    {
        $object = new ItemPackage();
        $object->setItem($item);
        
        return $object;
    }

    public function findSmallestUnitByItem(ItemInterface $item): ?ItemPackageInterface 
    {
        $packaging = (new Packaging())->setCode('pcs')->setName('PCS');
        
        return (new ItemPackage())
                ->setItem($item)
                ->setPackaging($packaging)
                ->setQuantity(1)
                ->setPrincipalPrice($item->getPrincipalPrice())
                ->setSalePrice($item->getLastPrice());
    }

    public function save(ItemPackageInterface $itemPackage): void 
    {
        
    }

}
