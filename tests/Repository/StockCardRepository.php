<?php

namespace Kematjaya\ItemPackBundle\Tests\Repository;

use Kematjaya\ItemPackBundle\Tests\Model\StockCard;
use Kematjaya\ItemPackBundle\Entity\StockCardInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockCardRepository implements \Kematjaya\ItemPackBundle\Repository\StockCardRepositoryInterface
{
    
    public function createStockCard(): StockCardInterface 
    {
        return new StockCard();
    }

    public function save(StockCardInterface $package): void 
    {
        
    }

}
