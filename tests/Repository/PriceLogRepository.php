<?php

namespace Kematjaya\ItemPackBundle\Tests\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceLogRepository implements \Kematjaya\ItemPackBundle\Repository\PriceLogRepositoryInterface
{
    
    public function createPriceLog(ItemInterface $item): PriceLogInterface 
    {
        $log = new \Kematjaya\ItemPackBundle\Tests\Model\PriceLog();
        
        return $log;
    }

    public function getNewPriceLogByItem(ItemInterface $item): ?PriceLogInterface 
    {
        return null;
    }

    public function getNewPriceLogs(): Collection 
    {
        return new ArrayCollection();
    }

    public function save(PriceLogInterface $priceLog): void 
    {
        
    }

}
