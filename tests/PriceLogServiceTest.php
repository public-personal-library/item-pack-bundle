<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Service\PriceService;
use Kematjaya\ItemPackBundle\Service\PriceLogServiceInterface;
use Kematjaya\ItemPackBundle\Repository\PriceLogRepositoryInterface;
use Kematjaya\ItemPackBundle\Repository\ItemPackageRepositoryInterface;
use Kematjaya\ItemPackBundle\Repository\ItemRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceLogServiceTest extends WebTestCase
{
    
    use ObjectTrait;
    
    public function testInstance():PriceLogServiceInterface
    {
        $client = static::createClient();
        $container = $client->getContainer();
        $this->assertTrue($container->has(PriceLogServiceInterface::class));
        
        return $container->get(PriceLogServiceInterface::class);
    }
    
    public static function getKernelClass() :string
    {
        return AppKernelTest::class;
    }
    
    public function testSaveNewPrice()
    {
        $itemRepository = $this->createMock(ItemRepositoryInterface::class);
        $itemPackageRepository = $this->createMock(ItemPackageRepositoryInterface::class);
        $priceLogRepository = $this->createMock(PriceLogRepositoryInterface::class);
        $priceLogRepository
                ->expects($this->once())
                ->method("createPriceLog")
                ->willReturn(
                    new Model\PriceLog()
                );
        
        $priceLogRepository
                ->expects($this->once())
                ->method("save");
        
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher
                ->expects($this->once())
                ->method("dispatch");
        
        $service = new PriceService(
            $itemRepository, 
            $itemPackageRepository, 
            $priceLogRepository, 
            $eventDispatcher
        );
        $item = $this->buildObject();
        
        $service->saveNewPrice($item, 1200);
    }
    
    public function testApprovePrice()
    {
        $itemRepository = $this->createMock(ItemRepositoryInterface::class);
        $itemRepository
                ->expects($this->once())
                ->method("save");
        
        $itemPackageRepository = $this->createMock(ItemPackageRepositoryInterface::class);
        $priceLogRepository = $this->createMock(PriceLogRepositoryInterface::class);
        $priceLogRepository
                ->expects($this->never())
                ->method("createPriceLog");

        $item = $this->buildObject();
        $priceLog = new Model\PriceLog();
        $priceLog->setItem($item);
        
        $priceLogRepository
                ->expects($this->once())
                ->method("save")
                ->with($priceLog);
        
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher
                ->expects($this->once())
                ->method("dispatch");
        
        $service = new PriceService(
            $itemRepository, 
            $itemPackageRepository, 
            $priceLogRepository, 
            $eventDispatcher
        );
        
        $service->approvePrice($priceLog);
    }
    
    public function testRejectedPrice()
    {
        $itemRepository = $this->createMock(ItemRepositoryInterface::class);
        $itemPackageRepository = $this->createMock(ItemPackageRepositoryInterface::class);
        $priceLogRepository = $this->createMock(PriceLogRepositoryInterface::class);
        $priceLogRepository
                ->expects($this->never())
                ->method("createPriceLog");
        
        $item = $this->buildObject();
        $priceLog = new Model\PriceLog();
        $priceLog->setItem($item);
        $priceLogRepository
                ->expects($this->once())
                ->method("save")
                ->with($priceLog);
        
        $eventDispatcher = $this->createMock(EventDispatcherInterface::class);
        $eventDispatcher
                ->expects($this->once())
                ->method("dispatch");
        
        $service = new PriceService(
            $itemRepository, 
            $itemPackageRepository, 
            $priceLogRepository, 
            $eventDispatcher
        );
        
        $service->rejectPrice($priceLog);
    }
}
