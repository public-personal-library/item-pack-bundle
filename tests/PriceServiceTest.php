<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Service\PriceServiceInterface;
use Kematjaya\ItemPackBundle\Exception\PackageEmptyException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceServiceTest extends WebTestCase 
{
    use ObjectTrait;
    
    public static function getKernelClass() :string
    {
        return AppKernelTest::class;
    }
    
    public function testInstance():PriceServiceInterface
    {
        $container = $this->getContainer();
        $this->assertTrue($container->has(PriceServiceInterface::class));
        
        return $container->get(PriceServiceInterface::class);
    }
    
    /**
     * @depends testInstance
     */
    public function testUpdatePrincipalPriceException(PriceServiceInterface $service)
    {
        $item = $this->buildObject();
        $packaging = $this->buildPackaging();
        
        $this->expectException(PackageEmptyException::class);
        $service->updatePrincipalPrice($item, $packaging, 1200);
    }
    
    /**
     * @depends testInstance
     */
    public function testUpdatePrincipalPriceSuccess(PriceServiceInterface $service)
    {
        $item = $this->buildObject();
        $item->addItemPackage($this->buildItemPackage($item));
        $packaging = $this->buildPackaging();
        
        $result = $service->updatePrincipalPrice($item, $packaging, 1500);
        $this->assertEquals(1500, $result->getPrincipalPrice());
    }
    
    /**
     * @depends testInstance
     */
    public function testUpdateSalePriceException(PriceServiceInterface $service)
    {
        $item = $this->buildObject();
        $packaging = $this->buildPackaging();
        
        $this->expectException(PackageEmptyException::class);
        $service->updateSalePrice($item, $packaging, 1200);
    }
    
    /**
     * @depends testInstance
     */
    public function testUpdateSalePriceSuccess(PriceServiceInterface $service)
    {
        $item = $this->buildObject();
        $item->addItemPackage($this->buildItemPackage($item));
        $packaging = $this->buildPackaging();
        
        $result = $service->updateSalePrice($item, $packaging, 1500);
        $this->assertEquals(1500, $result->getLastPrice());
    }
}
