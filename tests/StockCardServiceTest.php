<?php

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Service\StockCardServiceInterface;
use Kematjaya\ItemPackBundle\Entity\ClientStockCardInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockCardServiceTest extends WebTestCase
{
    use ObjectTrait;
    
    public static function getKernelClass() :string
    {
        return AppKernelTest::class;
    }
    
    public function testInstance():StockCardServiceInterface
    {
        $container = $this->getContainer();
        $this->assertTrue($container->has(StockCardServiceInterface::class));
        
        return $container->get(StockCardServiceInterface::class);
    }
    
    /**
     * @depends testInstance
     */
    public function testInsert(StockCardServiceInterface $service)
    {
        $item = $this->buildObject();
        $client = $this->createConfiguredMock(ClientStockCardInterface::class, [
            'getClassId' => (string) rand(),
            'getTypeTransaction' => ClientStockCardInterface::TYPE_ADD,
            'getQuantity' => (float)10
        ]);
        $stockCard = $service->insertStockCard($item, $client);
        $this->assertEquals($item->getLastStock(), $stockCard->getTotal());
        $this->assertEquals($client->getQuantity(), $stockCard->getQuantity());
    }
}
