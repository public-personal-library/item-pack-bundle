<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Tests;

use Kematjaya\ItemPackBundle\Exception\NotSufficientStockException;
use Kematjaya\ItemPackBundle\Entity\StoreInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Kematjaya\ItemPackBundle\Manager\StockManagerInterface;
use Kematjaya\ItemPackBundle\Tests\AppKernelTest;

/**
 * Description of StockManagerTest
 *
 * @author apple
 */
class StockManagerTest extends \Symfony\Bundle\FrameworkBundle\Test\WebTestCase 
{
    use ObjectTrait;
    
    public static function getKernelClass():string 
    {
        return AppKernelTest::class;
    }
    
    public function testInstance()
    {
        $container = static::getContainer();
        $this->assertTrue($container->has(StockManagerInterface::class));
    }
    
    public function testInsertStock()
    {
        $container = static::getContainer();
        $manager = $container->get(StockManagerInterface::class);
        if (!$manager instanceof StockManagerInterface) {
            return;
        }
        
        $item = $this->buildObject();
        $itemPackage = $this->buildItemPackage($item);
        $item->addItemPackage($itemPackage);
        
        $store = $this->createMock(StoreInterface::class);
        $quantity = (float) 10;
        $entity = $this->createMock(StoreStockCardTransactionInterface::class);
        $entity->expects($this->any())
                ->method("getPackaging")->willReturn($itemPackage->getPackaging());
        $entity->expects($this->any())
                ->method("getQuantity")->willReturn($quantity);
        $entity->expects($this->any())
                ->method("getStore")->willReturn($store);
        
        $itemStore = $manager->insertStock($item, $entity);
        
        $this->assertEquals($quantity, $itemStore->getQuantity());
    }
    
    public function testNotSufficientStockException()
    {
        $container = static::getContainer();
        $manager = $container->get(StockManagerInterface::class);
        if (!$manager instanceof StockManagerInterface) {
            return;
        }
        
        $item = $this->buildObject();
        $itemPackage = $this->buildItemPackage($item);
        $item->addItemPackage($itemPackage);
        
        $store = $this->createMock(StoreInterface::class);
        $quantity = (float) 10;
        $entity = $this->createMock(StoreStockCardTransactionInterface::class);
        $entity->expects($this->any())
                ->method("getPackaging")->willReturn($itemPackage->getPackaging());
        $entity->expects($this->any())
                ->method("getQuantity")->willReturn($quantity);
        $entity->expects($this->any())
                ->method("getStore")->willReturn($store);
        
        $itemStore = $manager->getStock($item, $entity);
        $this->assertEquals(0 - $quantity, $itemStore->getQuantity());
        
        $itemStock = $this->buildObjectStock();
        $itemPackageStock = $this->buildItemPackage($itemStock);
        $itemStock->addItemPackage($itemPackageStock);
        
        $entity = $this->createMock(StoreStockCardTransactionInterface::class);
        $entity->expects($this->any())
                ->method("getPackaging")->willReturn($itemPackageStock->getPackaging());
        $entity->expects($this->any())
                ->method("getQuantity")->willReturn($quantity);
        $entity->expects($this->any())
                ->method("getStore")->willReturn($store);
        
        $this->expectException(NotSufficientStockException::class);
        $manager->getStock($itemStock, $entity);
    }
}
