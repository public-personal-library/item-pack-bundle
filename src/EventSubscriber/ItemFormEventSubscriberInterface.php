<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 *
 * @author programmer
 */
interface ItemFormEventSubscriberInterface extends EventSubscriberInterface 
{
    
    const TAG_NAME = 'item_pack.form_event_subscriber';
    
    public function isSupported(string $className):bool;
}
