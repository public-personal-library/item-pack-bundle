<?php

namespace Kematjaya\ItemPackBundle\EventSubscriber;

use Kematjaya\ItemPackBundle\Entity\ItemCategoryInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemCategoryEventSubscriber implements ItemFormEventSubscriberInterface 
{
    
    public function isSupported(string $className):bool
    {
        $reflection = new \ReflectionClass($className);
        
        return $reflection->isSubclassOf(ItemCategoryInterface::class);
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::PRE_SET_DATA => 'preSetData',
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }
    
    public function preSetData(FormEvent $event):void
    {
        $itemCategory = $event->getData();
        if (!$itemCategory instanceof ItemCategoryInterface) {
            return;
        }
        
        if (!$itemCategory->getId()) {
            $itemCategory->setIsActive(true);
        }

        $event->setData($itemCategory);
    }
    
    public function postSubmit(FormEvent $event):void
    {
        $itemCategory = $event->getData();
        if (!$itemCategory instanceof ItemCategoryInterface) {
            return;
        }
        
        $itemCategory->setCode(trim($itemCategory->getCode()));
        $itemCategory->setName(trim($itemCategory->getName()));

        $event->setData($itemCategory);
    }
}
