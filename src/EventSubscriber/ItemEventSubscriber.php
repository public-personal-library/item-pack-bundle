<?php

namespace Kematjaya\ItemPackBundle\EventSubscriber;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\StockInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemEventSubscriber implements ItemFormEventSubscriberInterface 
{
    
    public function isSupported(string $className):bool
    {
        $reflection = new \ReflectionClass($className);
        
        return $reflection->isSubclassOf(ItemInterface::class);
    }
    
    public static function getSubscribedEvents():array
    {
        return [
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }
    
    public function postSubmit(FormEvent $event):void
    {
        $item = $event->getData();
        if ($item instanceof ItemInterface) {
            return;
        }
        
        $item->setCode(trim($item->getCode()));
        $item->setName(trim($item->getName()));
        if (!$item instanceof StockInterface) {
            
            $event->setData($item);
            return;
        }
        
        if (null === $item->getLastStock()) {
            $item->setLastStock(0);
        }

        $event->setData($item);
    }
}
