<?php

namespace Kematjaya\ItemPackBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Kematjaya\ItemPackBundle\Service\PriceLogServiceInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceLogEventSubscriber implements EventSubscriber
{
    
    /**
     * 
     * @var PriceLogServiceInterface
     */
    private $priceLogService;
    
    public function __construct(PriceLogServiceInterface $priceLogService) 
    {
        $this->priceLogService = $priceLogService;
    }
    
    public function getSubscribedEvents(): array
    {
        return [
            Events::onFlush
        ];
    }
    
    public function onFlush(OnFlushEventArgs $eventArgs):void
    {
        $em = $eventArgs->getEntityManager();
        $uow = $em->getUnitOfWork();
        
        foreach ($uow->getScheduledEntityInsertions() as $entity) {
            if ($entity instanceof PriceLogInterface) {
                $this->updateprice($entity);
            }
        }
        
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof PriceLogInterface) {
                $this->updatePrice($entity);
            }
        }
        
        foreach ($uow->getScheduledEntityDeletions() as $entity) {
            if ($entity instanceof PriceLogInterface) {
                $this->updatePrice($entity);
            }
        }
    }
    
    private function updatePrice(PriceLogInterface $entity):void
    {
        if (PriceLogInterface::STATUS_APPROVED === $entity->getStatus()) {
            $this->priceLogService->approvePrice($entity);
            return;
        }
        
        if (PriceLogInterface::STATUS_REJECTED === $entity->getStatus()) {
            $this->priceLogService->rejectPrice($entity);
            return;
        }
    }
}
