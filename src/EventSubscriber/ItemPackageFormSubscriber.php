<?php

namespace Kematjaya\ItemPackBundle\EventSubscriber;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormError;
use Kematjaya\ItemPackBundle\Repository\ItemPackageRepositoryInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackageFormSubscriber implements ItemFormEventSubscriberInterface 
{
    
    /**
     * 
     * @var ItemPackageRepositoryInterface
     */
    private $itemPackageRepo;
    
    public function __construct(ItemPackageRepositoryInterface $itemPackageRepo) 
    {
        $this->itemPackageRepo = $itemPackageRepo;
    }
    
    public function isSupported(string $className):bool
    {
        $reflection = new \ReflectionClass($className);
        
        return $reflection->isSubclassOf(ItemPackageInterface::class);
    }
    
    public static function getSubscribedEvents(): array 
    {
        return [
            FormEvents::POST_SUBMIT => 'postSubmit'
        ];
    }

    public function postSubmit(FormEvent $event):void
    {
        $data = $event->getData();
        if (!$data instanceof ItemPackageInterface) {
            return;
        }   
        
        $form = $event->getForm();
        if ($data->isSmallestUnit()) {
            $smallestUnit = $this->itemPackageRepo->findSmallestUnitByItem($data->getItem());
            if ($smallestUnit and $smallestUnit->getId() !== $data->getId()) {
                $form->addError(new FormError('kemasan terkecil tidak boleh lebih dari 1'));
                return;
            }

            $data->setQuantity(1);
        } else {
            $data->setIsSmallestUnit(false);
        }

        if ($data->getPrincipalPrice()) {
            $data->setPrincipalPrice(0);
        }
        
        if ($data->getSalePrice()) {
            $data->setSalePrice(0);
        }

        $event->setData($data);
    }
}
