<?php

namespace Kematjaya\ItemPackBundle\Exception;

use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 * Description of ItemPackageNotFoundException
 *
 * @author apple
 */
class ItemPackageNotFoundException extends \Exception
{
    public function __construct(PackagingInterface $package, ItemInterface $item)
    {
        $message = sprintf("package '%s' for item '%s' not found", $package->getName(), $item->getName());
        
        return parent::__construct($message);
    }
}
