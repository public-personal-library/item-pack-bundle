<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Event;

use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Symfony\Contracts\EventDispatcher\Event;
 
/**
 * Description of PriceChangeEvent
 *
 * @author programmer
 */
class PriceChangeEvent extends Event
{
    const EVENT_NAME = "price_changed";
    
    /**
     * 
     * @var PriceLogInterface
     */
    private $entity;
    
    public function __construct(PriceLogInterface $entity) 
    {
        $this->entity = $entity;
    }
    
    public function getEntity(): PriceLogInterface 
    {
        return $this->entity;
    }

}
