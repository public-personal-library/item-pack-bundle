<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Event;

use Kematjaya\ItemPackBundle\Entity\StoreInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PostInsertStock
 *
 * @author apple
 */
class PostInsertStockEvent extends Event 
{
    
    const EVENT_NAME = "item.post_insert_stock";
    
    private StoreStockCardTransactionInterface $entity;
    
    private StoreInterface $store;
    
    private float $quantity;
    
    public function __construct(StoreStockCardTransactionInterface $entity, StoreInterface $store, float $quantity) 
    {
        $this->entity = $entity;
        $this->store = $store;
        $this->quantity = $quantity;
    }
    
    public function getEntity(): StoreStockCardTransactionInterface 
    {
        return $this->entity;
    }

    public function getStore(): StoreInterface 
    {
        return $this->store;
    }

    public function getQuantity(): float 
    {
        return $this->quantity;
    }


}
