<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Event;

use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Description of PriceApproveEvent
 *
 * @author programmer
 */
class PriceApproveEvent extends Event
{
    const EVENT_NAME = "price_approved";
    
    /**
     * 
     * @var PriceLogInterface
     */
    private $entity;
    
    public function __construct(PriceLogInterface $entity) 
    {
        $this->entity = $entity;
    }
    
    public function getEntity(): PriceLogInterface 
    {
        return $this->entity;
    }

}