<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Builder;

use Kematjaya\ItemPackBundle\EventSubscriber\ItemFormEventSubscriberInterface;
use Doctrine\Common\Collections\Collection;

/**
 *
 * @author programmer
 */
interface FormEventSubscriberBuilderInterface 
{
    public function addFormSubscriber(ItemFormEventSubscriberInterface $element): self;

    public function getFormSubscribers(string $className):Collection;
}
