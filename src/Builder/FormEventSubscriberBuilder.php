<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Builder;

use Kematjaya\ItemPackBundle\EventSubscriber\ItemFormEventSubscriberInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of FormEventSubscriberBuilder
 *
 * @author programmer
 */
class FormEventSubscriberBuilder implements FormEventSubscriberBuilderInterface 
{
    /**
     * 
     * @var Collection
     */
    private $elements;
    
    public function __construct() 
    {
        $this->elements = new ArrayCollection();
    }
    
    public function addFormSubscriber(ItemFormEventSubscriberInterface $element): FormEventSubscriberBuilderInterface 
    {
        if (!$this->elements->contains($element)) {
            $this->elements->add($element);
        }
        
        return $this;
    }

    public function getFormSubscribers(string $className): Collection 
    {
        return $this->elements->filter(function (ItemFormEventSubscriberInterface $element) use ($className) {
            return $element->isSupported($className);
        });
    }

}
