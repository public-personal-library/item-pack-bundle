<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Entity;

use Kematjaya\ItemPackBundle\Entity\StockCardInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;

/**
 *
 * @author apple
 */
interface ClientStockCardInterface 
{
    const TYPE_GET = StockCardInterface::TYPE_GET;
    const TYPE_ADD = StockCardInterface::TYPE_ADD;
    
    public function getClassId():?string;
    
    public function getTypeTransaction():string;
    
    public function getQuantity():?float;
    
    public function getPackaging(): ?PackagingInterface;
}
