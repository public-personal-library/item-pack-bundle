<?php

namespace Kematjaya\ItemPackBundle\Entity;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PriceLogInterface 
{
    const STATUS_NEW = 1;
    const STATUS_APPROVED = 2;
    const STATUS_REJECTED = 0;
    
    public function isNew():bool;
    
    public function isApproved():bool;
    
    public function isRejected():bool;
    
    public function getItem():?ItemInterface;
    
    public function getCreatedAt():?\DateTimeInterface;
    
    public function getPrincipalPriceOld():?float;
    
    public function getPrincipalPrice():?float;
    
    public function getSalePriceOld():?float;
    
    public function getSalePrice():?float;
    
    public function getStatus():int;
}
