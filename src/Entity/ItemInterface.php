<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Kematjaya\ItemPackBundle\Entity\ItemCategoryInterface;

/**
 *
 * @author apple
 */
interface ItemInterface 
{
    
    public function getCode(): ?string;

    public function getName(): ?string;

    public function getCategory(): ?ItemCategoryInterface;

    public function getLastStock(): ?float;
    
    public function getItemPackages(): Collection;
    
}
