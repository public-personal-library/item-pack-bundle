<?php

namespace Kematjaya\ItemPackBundle\Entity;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PriceLogClientInterface 
{
    public function getActiveSalePrice(): ?float;
    
    public function getActivePrincipalPrice(): ?float;
}
