<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Entity;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 *
 * @author apple
 */
interface StockCardInterface 
{
    const TYPE_GET = 'GET';
    const TYPE_ADD = 'ADD';
    
    public function getItem():ItemInterface;
    
    public function getCreatedAt():\DateTimeInterface;
    
    public function getClassName():string;
    
    public function getClassId():string;
    
    public function getQuantity(): float;
    
    public function getType():string;
    
    public function getTotal():float;
    
}
