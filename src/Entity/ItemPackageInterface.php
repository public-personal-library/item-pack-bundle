<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Entity;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;

/**
 *
 * @author apple
 */
interface ItemPackageInterface 
{
    public function getItem():ItemInterface;
    
    public function getPackaging():?PackagingInterface;
    
    public function getPrincipalPrice():?float;
    
    public function getSalePrice():?float;
    
    public function getQuantity():?float;
    
    public function isSmallestUnit():bool;
}
