<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Entity;

/**
 *
 * @author apple
 */
interface ItemStoreInterface 
{
    public function getItem():?ItemInterface;
    
    public function getStore():?StoreInterface;
    
    public function getQuantity():float;
}
