<?php

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Exception;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackageRepository implements ItemPackageRepositoryInterface
{
    
    public function createPackage(ItemInterface $item): ItemPackageInterface 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", ItemPackageRepoInterface::class)
        );
    }

    public function findSmallestUnitByItem(ItemInterface $item): ?ItemPackageInterface 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", ItemPackageRepoInterface::class)
        );
    }

    public function save(ItemPackageInterface $itemPackage): void 
    {
        throw new Exception(
            sprintf("please implement interface %s with your class", ItemPackageRepoInterface::class)
        );
    }

}
