<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Entity\StoreInterface;
use Kematjaya\ItemPackBundle\Entity\ItemStoreInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;

/**
 *
 * @author apple
 */
interface ItemStoreRepositoryInterface 
{
    public function findOneByItemAndStore(ItemPackageInterface $itemPackage, StoreInterface $store):?ItemStoreInterface;
    
    public function save(ItemStoreInterface $itemStore, float $quantity, StoreStockCardTransactionInterface $entity = null):void;
}
