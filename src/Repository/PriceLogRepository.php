<?php

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Doctrine\Common\Collections\Collection;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceLogRepository implements PriceLogRepositoryInterface
{
    
    public function createPriceLog(ItemInterface $item): PriceLogInterface 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", PriceLogRepoInterface::class)
        );
    }

    public function getNewPriceLogByItem(ItemInterface $item): ?PriceLogInterface 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", PriceLogRepoInterface::class)
        );
    }

    public function getNewPriceLogs(): Collection 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", PriceLogRepoInterface::class)
        );
    }

    public function save(PriceLogInterface $priceLog): void 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", PriceLogRepoInterface::class)
        );
    }

}
