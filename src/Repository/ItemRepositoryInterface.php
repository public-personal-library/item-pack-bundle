<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 *
 * @author apple
 */
interface ItemRepositoryInterface 
{    
    public function createItem():ItemInterface;
    
    public function save(ItemInterface $item):void;
}
