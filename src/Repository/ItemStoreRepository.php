<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPClass.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Entity\StoreInterface;
use Kematjaya\ItemPackBundle\Entity\ItemStoreInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;

/**
 * Description of ItemStoreRepository
 *
 * @author apple
 */
class ItemStoreRepository implements ItemStoreRepositoryInterface 
{
    //put your code here
    public function findOneByItemAndStore(ItemPackageInterface $itemPackage, StoreInterface $store): ?ItemStoreInterface 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", ItemStoreRepositoryInterface::class)
        );
    }

    public function save(ItemStoreInterface $itemStore, float $quantity, StoreStockCardTransactionInterface $entity = null): void 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", ItemStoreRepositoryInterface::class)
        );
    }

}
