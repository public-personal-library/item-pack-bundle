<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;

/**
 *
 * @author apple
 */
interface ItemPackageRepositoryInterface 
{
    public function createPackage(ItemInterface $item): ItemPackageInterface;

    public function findSmallestUnitByItem(ItemInterface $item): ?ItemPackageInterface;

    public function save(ItemPackageInterface $itemPackage): void;
}
