<?php

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Doctrine\Common\Collections\Collection;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PriceLogRepositoryInterface
{
    
    public function createPriceLog(ItemInterface $item):PriceLogInterface;
    
    public function save(PriceLogInterface $priceLog): void;
    
    /**
     * Get one price log by item where status is new
     * @param ItemInterface $item
     * @return PriceLogInterface|null
     */
    public function getNewPriceLogByItem(ItemInterface $item):?PriceLogInterface;
    
    /**
     * Get all price log where status is new
     * @return Collection|PriceLogInterface
     */
    public function getNewPriceLogs():Collection;
}
