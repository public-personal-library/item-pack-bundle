<?php

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemRepository implements ItemRepositoryInterface
{
    
    public function createItem(): ItemInterface 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", ItemInterface::class)
        );
    }

    public function save(ItemInterface $item): void 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", ItemInterface::class)
        );
    }

}
