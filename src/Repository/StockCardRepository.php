<?php

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\StockCardInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockCardRepository implements StockCardRepositoryInterface
{
    
    public function createStockCard(): StockCardInterface 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", StockCardRepoInterface::class)
        );
    }

    public function save(StockCardInterface $package): void 
    {
        throw new \Exception(
            sprintf("please implement interface %s with your class", StockCardRepoInterface::class)
        );
    }

}
