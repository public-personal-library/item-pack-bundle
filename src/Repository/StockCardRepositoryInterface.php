<?php

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/PHPInterface.php to edit this template
 */

namespace Kematjaya\ItemPackBundle\Repository;

use Kematjaya\ItemPackBundle\Entity\StockCardInterface;

/**
 *
 * @author apple
 */
interface StockCardRepositoryInterface 
{
    
    public function createStockCard(): StockCardInterface;

    public function save(StockCardInterface $package): void;
}
