<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Exception\PackageEmptyException;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
trait ServiceTrait 
{
    protected function getItemPackByPackagingOrSmallestUnit(ItemInterface $item, PackagingInterface $packaging = null):?ItemPackageInterface
    {
        if ($item->getItemPackages()->isEmpty()) {
            throw new PackageEmptyException(get_class($item));
        }

        $itemPackages = $item->getItemPackages()->filter(function (ItemPackageInterface $itemPackage) use ($packaging) {
            if ($packaging) {
                return $packaging->getCode() === $itemPackage->getPackaging()->getCode();
            }

            return $itemPackage->isSmallestUnit();
        })->first();

        return $itemPackages instanceof ItemPackageInterface ? $itemPackages : null;
    }
}
