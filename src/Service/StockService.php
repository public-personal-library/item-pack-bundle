<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Exception\NotSufficientStockException;
use Kematjaya\ItemPackBundle\Repository\ItemRepositoryInterface;
use Kematjaya\ItemPackBundle\Entity\StockInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;

/**
 * @deprecated use Kematjaya\ItemPackBundle\Manager\StockManagerInterface
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockService implements StockServiceInterface
{   
    /**
     * 
     * @var ItemRepositoryInterface
     */
    protected $itemRepo;
    
    use ServiceTrait;
    
    public function __construct(ItemRepositoryInterface $itemRepo) 
    {
        $this->itemRepo = $itemRepo;
    }
    
    /**
     * @deprecated since version 1.2
     * @param ItemInterface $item
     * @param float $quantity
     * @param PackagingInterface $packaging
     * @return ItemInterface
     */
    public function updateStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface
    {
        $itemPack = $item->getItemPackages()->filter(function (ItemPackageInterface $itemPackage) use ($packaging) {
            if($packaging)
            {
                return $packaging->getCode() === $itemPackage->getPackaging()->getCode();
            }
            return $itemPackage->isSmallestUnit();
        })->first();
        
        if($itemPack instanceof ItemPackageInterface) {
            $quantity = $quantity * $itemPack->getQuantity();
        }
        
        return $item;
    }
    
    public function addStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface
    {
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        
        if($itemPack instanceof ItemPackageInterface) {
            $quantity = ($itemPack->isSmallestUnit()) ? $quantity : $quantity * $itemPack->getQuantity();
        }
        
        $lastStock = $item->getLastStock() + $quantity;
        $item->setLastStock($lastStock);
        
        $this->itemRepo->save($item);
        
        return $item;
    }
    
    public function getStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface
    {
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        
        if($itemPack instanceof ItemPackageInterface) {
            $quantity = ($itemPack->isSmallestUnit()) ? $quantity : $quantity * $itemPack->getQuantity();
        }
        
        $lastStock = $item->getLastStock() - $quantity;
        if ($item instanceof StockInterface) {
            if (true === $item->isCountStock() and $lastStock < 0) {
                throw new NotSufficientStockException(
                    $item->getLastStock(), 
                    $quantity
                );
            }
        }
            
        $item->setLastStock($lastStock);
        
        $this->itemRepo->save($item);
        
        return $item;
    }
}
