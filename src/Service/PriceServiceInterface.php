<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PriceServiceInterface 
{
    public function updatePrincipalPrice(ItemInterface $item, PackagingInterface $packaging, float $price = 0):ItemInterface;
    
    public function updateSalePrice(ItemInterface $item, PackagingInterface $packaging, float $price = 0):ItemInterface;
    
}
