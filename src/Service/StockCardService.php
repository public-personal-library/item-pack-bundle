<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\StockCardInterface;
use Kematjaya\ItemPackBundle\Entity\ClientStockCardInterface;
use Kematjaya\ItemPackBundle\Repository\StockCardRepositoryInterface;
use Kematjaya\ItemPackBundle\Service\StockCardServiceInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class StockCardService implements StockCardServiceInterface
{
    /**
     * 
     * @var StockCardRepositoryInterface
     */
    protected $stockCardRepo;
    
    use ServiceTrait;
    
    public function __construct(StockCardRepositoryInterface $stockCardRepo) 
    {
        $this->stockCardRepo = $stockCardRepo;
    }
    
    public function insertStockCard(ItemInterface $item, ClientStockCardInterface $entity):StockCardInterface
    {
        $stockCard = $this->stockCardRepo->createStockCard();
        $stockCard->setCreatedAt(new \DateTime())
                ->setItem($item)
                ->setQuantity($entity->getQuantity())
                ->setType($entity->getTypeTransaction())
                ->setTotal($item->getLastStock())
                ->setClassName(get_class($entity))
                ->setClassId($entity->getClassId());
        
        $this->stockCardRepo->save($stockCard);
        
        return $stockCard;
    }
}
