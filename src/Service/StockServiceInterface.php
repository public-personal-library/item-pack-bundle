<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;

/**
 * @deprecated use Kematjaya\ItemPackBundle\Manager\StockManagerInterface
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface StockServiceInterface 
{
    /**
     * @deprecated since version 1.2
     * @param ItemInterface $item
     * @param float $quantity
     * @param PackagingInterface $packaging
     * @return ItemInterface
     */
    public function updateStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface;
    
    /**
     * since version 1.2
     * @param ItemInterface $item
     * @param float $quantity
     * @param PackagingInterface $packaging
     * @return ItemInterface
     */
    public function addStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface;
    
    /**
     * since version 1.2
     * @param ItemInterface $item
     * @param float $quantity
     * @param PackagingInterface $packaging
     * @return ItemInterface
     */
    public function getStock(ItemInterface $item, float $quantity = 0, PackagingInterface $packaging = null):ItemInterface;
}
