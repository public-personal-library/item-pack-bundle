<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ClientStockCardInterface;
use Kematjaya\ItemPackBundle\Entity\StockCardInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface StockCardServiceInterface 
{    
    public function insertStockCard(ItemInterface $item, ClientStockCardInterface $entity):StockCardInterface;
}
