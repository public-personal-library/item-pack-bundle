<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
interface PriceLogServiceInterface 
{
    public function saveNewPrice(ItemInterface $item, float $price = 0):void;
    
    public function approvePrice(PriceLogInterface $priceLog):void;
    
    public function rejectPrice(PriceLogInterface $priceLog):void;
}
