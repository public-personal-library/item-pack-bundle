<?php

namespace Kematjaya\ItemPackBundle\Service;

use Kematjaya\ItemPackBundle\Event\PriceChangeEvent;
use Kematjaya\ItemPackBundle\Event\PriceRejectedEvent;
use Kematjaya\ItemPackBundle\Event\PriceApproveEvent;
use Kematjaya\ItemPackBundle\Service\PriceServiceInterface;
use Kematjaya\ItemPackBundle\Service\PriceLogServiceInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\PackagingInterface;
use Kematjaya\ItemPackBundle\Entity\ItemPackageInterface;
use Kematjaya\ItemPackBundle\Repository\ItemRepositoryInterface;
use Kematjaya\ItemPackBundle\Repository\ItemPackageRepositoryInterface;
use Kematjaya\ItemPackBundle\Repository\PriceLogRepositoryInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogInterface;
use Kematjaya\ItemPackBundle\Entity\PriceLogClientInterface;
use Kematjaya\ItemPackBundle\Exception\PackageEmptyException;
use Kematjaya\ItemPackBundle\Exception\SmallestPackageNotFoundException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class PriceService implements PriceServiceInterface, PriceLogServiceInterface
{
    use ServiceTrait;
    
    /**
     * 
     * @var ItemRepositoryInterface
     */
    private $itemRepo;
    
    /**
     * 
     * @var ItemPackageRepositoryInterface
     */
    private $itemPackageRepo;
    
    /**
     * 
     * @var PriceLogRepositoryInterface
     */
    private $priceLogRepo;
    
    /**
     * 
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    
    public function __construct(ItemRepositoryInterface $itemRepo, ItemPackageRepositoryInterface $itemPackageRepo, PriceLogRepositoryInterface $priceLogRepo, EventDispatcherInterface $eventDispatcher) 
    {
        $this->itemRepo = $itemRepo;
        $this->itemPackageRepo = $itemPackageRepo;
        $this->priceLogRepo = $priceLogRepo;
        $this->eventDispatcher = $eventDispatcher;
    }
    
    public function updatePrincipalPrice(ItemInterface $item, PackagingInterface $packaging, float $price = 0):ItemInterface
    {
        $this->validate($item);
        
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        
        if (!$itemPack instanceof ItemPackageInterface) {
            throw new SmallestPackageNotFoundException($item);
        }
        
        $itemPack->setPrincipalPrice($price);
        if ($itemPack->isSmallestUnit()) {
            $item->setPrincipalPrice($price);
        }

        $this->itemPackageRepo->save($itemPack);
        $this->itemRepo->save($item);
        
        return $item;
    }
    
    public function updateSalePrice(ItemInterface $item, PackagingInterface $packaging, float $price = 0):ItemInterface
    {
        $this->validate($item);
        
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        if (!$itemPack instanceof ItemPackageInterface) {
            throw new SmallestPackageNotFoundException($item);
        }
        
        $itemPack->setSalePrice($price);
        if ($itemPack->isSmallestUnit()) {
            $item->setLastPrice($price);
        }

        $this->itemPackageRepo->save($itemPack);
        $this->itemRepo->save($item);
        
        return $item;
    }
    
    /**
     * Save Price if Principal Price != Old Principal Price
     * @param ItemInterface $item
     * @param float $price
     * @return PriceLogInterface|null
     */
    public function saveNewPrice(ItemInterface $item, float $price = 0):void
    {
        if(!$item instanceof PriceLogClientInterface) {
            return;
        }
        
        if($price == $item->getPrincipalPrice()) {
            return;
        }
        
        $priceLog = $this->priceLogRepo->createPriceLog($item);
        $priceLog
                ->setCreatedAt(new \DateTime())
                ->setItem($item)
                ->setStatus(PriceLogInterface::STATUS_NEW)
                ->setPrincipalPrice($price)
                ->setSalePrice($price);
        if($item->getActivePrincipalPrice()) {
            $priceLog->setPrincipalPriceOld($item->getActivePrincipalPrice());
        }
        
        if($item->getActiveSalePrice()) {
            $priceLog->setSalePriceOld($item->getActiveSalePrice());
        }

        $this->priceLogRepo->save($priceLog);
        $this->eventDispatcher->dispatch(
            new PriceChangeEvent($priceLog),
            PriceChangeEvent::EVENT_NAME
        );
    }
    
    public function approvePrice(PriceLogInterface $priceLog):void
    {
        $item = $priceLog->getItem();
        $item->setPrincipalPrice((float)$priceLog->getPrincipalPrice());
        $item->setLastPrice((float)$priceLog->getSalePrice());
        
        $priceLog->setStatus(PriceLogInterface::STATUS_APPROVED);
        
        $this->priceLogRepo->save($priceLog);
        
        $this->itemRepo->save($item);
        
        $this->eventDispatcher->dispatch(
            new PriceApproveEvent($priceLog),
            PriceApproveEvent::class
        );
    }
    
    public function rejectPrice(PriceLogInterface $priceLog):void
    {
        $priceLog->setStatus(PriceLogInterface::STATUS_REJECTED);
        
        $this->priceLogRepo->save($priceLog);
        
        $this->eventDispatcher->dispatch(
            new PriceRejectedEvent($priceLog),
            PriceRejectedEvent::class
        );
    }
    
    protected function validate(ItemInterface $item):bool
    {
        if ($item->getItemPackages()->isEmpty()) {
            
            throw new PackageEmptyException(get_class($item));
        }
        
        return true;
    }
}
