<?php

namespace Kematjaya\ItemPackBundle\Manager;

use Kematjaya\ItemPackBundle\Entity\ItemStoreInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;

/**
 *
 * @author apple
 */
interface StockManagerInterface {
    
    public function insertStock(ItemInterface $item, StoreStockCardTransactionInterface $entity):ItemStoreInterface;
    
    public function getStock(ItemInterface $item, StoreStockCardTransactionInterface $entity):ItemStoreInterface;
}
