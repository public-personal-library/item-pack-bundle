<?php

namespace Kematjaya\ItemPackBundle\Manager;

use Kematjaya\ItemPackBundle\Exception\ItemPackageNotFoundException;
use Kematjaya\ItemPackBundle\Exception\NotSufficientStockException;
use Kematjaya\ItemPackBundle\Event\PostInsertStockEvent;
use Kematjaya\ItemPackBundle\Event\PostGetStockEvent;
use Kematjaya\ItemPackBundle\Entity\StockInterface;
use Kematjaya\ItemPackBundle\Entity\ItemInterface;
use Kematjaya\ItemPackBundle\Entity\ItemStoreInterface;
use Kematjaya\ItemPackBundle\Entity\StoreStockCardTransactionInterface;
use Kematjaya\ItemPackBundle\Service\ServiceTrait;
use Kematjaya\ItemPackBundle\Repository\ItemStoreRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of StockManager
 *
 * @author apple
 */
class StockManager implements StockManagerInterface 
{
    use ServiceTrait;
    
    private ItemStoreRepositoryInterface $itemStoreRepository;
    
    private EventDispatcherInterface $eventDispatcher;
    
    public function __construct(ItemStoreRepositoryInterface $itemStoreRepository, EventDispatcherInterface $eventDispatcher) 
    {
        $this->itemStoreRepository = $itemStoreRepository;
        $this->eventDispatcher = $eventDispatcher;
    }
    
    public function getStock(ItemInterface $item, StoreStockCardTransactionInterface $entity): ItemStoreInterface 
    {
        $packaging = $entity->getPackaging();
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        if (null === $itemPack) {
            throw new ItemPackageNotFoundException($packaging, $item);
        }
        
        $itemStore = $this->itemStoreRepository->findOneByItemAndStore(
            $itemPack, 
            $entity->getStore()
        );
        $stock = null !== $itemStore ? $itemStore->getQuantity() : 0;
        $quantity = $entity->getQuantity();
        
        $lastStock = $stock - $quantity;
        if ($item instanceof StockInterface) {
            if ($item->isCountStock() && $lastStock < 0) {
                throw new NotSufficientStockException(
                    $stock, 
                    $quantity
                );
            }
        }
        
        $this->itemStoreRepository->save($itemStore, $lastStock, $entity);
        
        $this->eventDispatcher->dispatch(
            new PostGetStockEvent($entity, $entity->getStore(), $quantity),
            PostGetStockEvent::EVENT_NAME
        );
        
        return $itemStore;
    }

    public function insertStock(ItemInterface $item, StoreStockCardTransactionInterface $entity): ItemStoreInterface 
    {
        $packaging = $entity->getPackaging();
        $itemPack = $this->getItemPackByPackagingOrSmallestUnit($item, $packaging);
        if (null === $itemPack) {
            throw new ItemPackageNotFoundException($packaging, $item);
        }
        
        $itemStore = $this->itemStoreRepository->findOneByItemAndStore(
            $itemPack, 
            $entity->getStore()
        );
        
        $stock = null !== $itemStore ? $itemStore->getQuantity() : 0;
        $quantity = $entity->getQuantity();
        
        $lastStock = $stock + $quantity;
        
        $this->itemStoreRepository->save($itemStore, $lastStock, $entity);
        
        $this->eventDispatcher->dispatch(
            new PostInsertStockEvent($entity, $entity->getStore(), $quantity),
            PostInsertStockEvent::EVENT_NAME
        );
        
        return $itemStore;
    }

}
