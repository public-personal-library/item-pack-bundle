<?php

namespace Kematjaya\ItemPackBundle;

use Kematjaya\ItemPackBundle\EventSubscriber\ItemFormEventSubscriberInterface;
use Kematjaya\ItemPackBundle\CompilerPass\FormEventSubscriberCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Nur Hidayatullah <kematjaya0@gmail.com>
 */
class ItemPackBundle extends Bundle 
{
    public function build(ContainerBuilder $container) 
    {
        $container->registerForAutoconfiguration(ItemFormEventSubscriberInterface::class)
                ->addTag(ItemFormEventSubscriberInterface::TAG_NAME);
        
        $container->addCompilerPass(new FormEventSubscriberCompilerPass());
        
        parent::build($container);
    }
}
